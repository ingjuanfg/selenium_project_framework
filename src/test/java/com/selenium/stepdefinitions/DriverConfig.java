package com.selenium.stepdefinitions;

import com.selenium.enums.Browser;
import com.selenium.util.DriverFactory;
import com.selenium.util.EnvironmentManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.*;

import java.time.Duration;

import static com.selenium.util.EnvironmentManager.urlEnvironment;

@Configuration
@ComponentScan(basePackages = "com.selenium")
@PropertySource("classpath:/${driver:chrome}.properties")
public class DriverConfig {

    @Value("${driver.type}")
    private Browser driverType;

    @Value("${element.wait.timeout.seconds}")
    private  int  webDriverWaitTimeOut;

    @Value("${nombre}")
    private  String  nombre;

   // @Value("${environment}")
  //  String environment;

    @Bean
    @Scope("cucumber-glue")
    public WebDriver webDriver(){
        //WebDriver driver;

        return DriverFactory.get(driverType);
    }

    @Bean
    @Scope("cucumber-glue")
    public WebDriverWait waitFor(){
        return  new WebDriverWait(webDriver(), Duration.ofSeconds(webDriverWaitTimeOut));
    }

    @Bean
    @Scope("cucumber-glue")
    public  String environment(){
        return  urlEnvironment(System.getProperty("environment"));
    }

    @Bean
    @Scope("cucumber-glue")
    public String nombre(){
        return  nombre;
    }

}
