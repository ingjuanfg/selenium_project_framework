package com.selenium.stepdefinitions;

import com.selenium.pages.SignUpServices;
import io.cucumber.java.After;
import io.cucumber.java.en.Given;
import io.cucumber.spring.CucumberContextConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.test.context.ContextConfiguration;

@CucumberContextConfiguration
@ContextConfiguration(classes = {DriverConfig.class})
public class SignUpStepDefs {

    @Autowired
    private SignUpServices signUp;

    @Autowired
    private  String environment;

     @Autowired
     private  String nombre;

    @Given("Pepito wants to have an account")
    public void pepito_wants_to_have_an_account() throws InterruptedException {
        System.out.println("valor nueva variable  " + nombre);
        signUp.go(environment);
    }

}
