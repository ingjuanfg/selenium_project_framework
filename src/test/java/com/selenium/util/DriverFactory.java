package com.selenium.util;

import com.selenium.enums.Browser;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;


public class DriverFactory {

    public  static WebDriver get(Browser browser){

       if(Browser.chrome == browser){
           WebDriverManager.chromedriver().setup();
           ChromeOptions chromeOptions = new ChromeOptions();
           chromeOptions.addArguments("['start-maximized']");
           return new ChromeDriver(chromeOptions);
       }

        if(Browser.firefox == browser){
            WebDriverManager.firefoxdriver().setup();
            return  new FirefoxDriver();
        }

        throw  new IllegalArgumentException("Driver not found browser" + browser);
    }
}
